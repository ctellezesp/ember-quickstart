import Route from '@ember/routing/route';

export default Route.extend({
	model(){
		return ['Spiderman', 'Thor', 'Captain America', 'Iron Man', 'Captain Marvel'];
	}
});
